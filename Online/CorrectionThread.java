/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package online;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javax.swing.JOptionPane;

/**
 *
 * @author bashimaislam
 */
public class CorrectionThread extends Thread{
    
    Socket mSocket;
        Exam exam;
	DataInputStream din;
	DataOutputStream dout;

    public CorrectionThread(Socket ClientSoc, Exam exam) {
        try
		{
                    
			mSocket = ClientSoc;
			din=new DataInputStream(ClientSoc.getInputStream());
			dout=new DataOutputStream(ClientSoc.getOutputStream());
			
                        this.exam = exam;
			start(); 

		}
		catch(IOException ex)
		{
                    System.out.println("FTP Client Connection Error: "+ex);
		}
    }

    @Override
	public void run()
	{
		while(true)
		{
			try
			{
                            System.out.println("Waiting for Correction Command ...");
                            String Command=din.readUTF();
                            if(Command.equals("CORRECTION"))
                            {
                                System.out.println("entered correction");
                                    String Correction = din.readUTF();
                                    System.out.println("correction: "+ Correction);
                                    JOptionPane.showMessageDialog(null, Correction, "New Correction", MAX_PRIORITY);
                            }
                            
                        
			}
			catch(Exception ex)
			{
                            System.out.println("run error: "+ex);
			}
		}
	}
    
        
        
    
}
