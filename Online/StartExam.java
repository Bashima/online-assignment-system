/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package online;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static online.ExamInformation.exam;
import static online.ExamInformation.mSocket;

/**
 *
 * @author bashimaislam
 */
public class StartExam extends TimerTask{
     private String mFileName;
     private Socket mSocket;
     private Exam mExam;
     public Timer timer1, timer2, timer3;

    public StartExam(String mFileName, Socket mSocket, Exam mExam) {
        this.mFileName = mFileName;
        this.mSocket = mSocket;
        this.mExam = mExam;
        timer1 = new Timer();
        timer2= new Timer();
        timer3 = new Timer();
        
    }

    @Override
    public void run() {
        try {
            
            Date date = new SimpleDateFormat("MM/dd/yyyy hh:mm").parse(exam.getmStartTime());
                DataInputStream din;
                DataOutputStream dout;
                BufferedReader br;
                
                din = new DataInputStream(mSocket.getInputStream());
                dout = new DataOutputStream(mSocket.getOutputStream());
                br = new BufferedReader(new InputStreamReader(System.in));
                dout.writeUTF("GET");
                
                String msgFromServer = din.readUTF();
                
                if (msgFromServer.compareTo("READY") == 0) {
                    try {
                        System.out.println("Receiving File ...");
                        File f = new File(mFileName);
                        if(!f.exists())
                        {
                            f.createNewFile();
                        }
                        try (FileOutputStream fout = new FileOutputStream(f)) {
                            int ch;
                            String temp;
                            do {
                                temp = din.readUTF();
                                ch = Integer.parseInt(temp);
                                if (ch != -1) {
                                    fout.write(ch);
                                }
                            } while (ch != -1);
                            JOptionPane.showMessageDialog(null, "start exam");
                            Date date_here = new Date();
                            UploadThread mBackUp = new UploadThread(mSocket, f, exam, "BACKUP");
                            UploadThread mFinalSubmission = new UploadThread(mSocket, f, exam, "FINISH");
                            Warning warning = new Warning(mExam);
                            
                            timer1.scheduleAtFixedRate(mBackUp, 0 , exam.getmBackupInterval()*60*1000);
                            timer2.schedule(mFinalSubmission,(exam.getmDuration()*60*1000 + date.getTime()- date_here.getTime()));
                            timer3.schedule(warning, (exam.getmDuration()*60*1000 + date.getTime()- date_here.getTime() -exam.getmWarning()*60*1000));
                            
                            
                            
                            //new CorrectionThread(mSocket, exam);
                            BasicFileAttributes attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
                            System.out.println("creationTime: " + attr.creationTime());
                            System.out.println("lastAccessTime: " + attr.lastAccessTime());
                            System.out.println("lastModifiedTime: " + attr.lastModifiedTime());
                            FileTime time= attr.creationTime();
                            exam.setmCreationTimeFirst(time);
                        } catch (IOException ex) {
                            Logger.getLogger(ExamInformation.class.getName()).log(Level.SEVERE, null, ex);
                        } 
                        //JOptionPane.showMessageDialog(null,din.readUTF() );
                    } catch (IOException ex) {
                        Logger.getLogger(ExamInformation.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ExamInformation.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
             Logger.getLogger(StartExam.class.getName()).log(Level.SEVERE, null, ex);
         }}
     
     
}
