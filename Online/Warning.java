/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package online;

import java.util.Date;
import java.util.TimerTask;
import javax.swing.JOptionPane;

/**
 *
 * @author bashimaislam
 */
public class Warning extends TimerTask{

    public Exam mExam;

    public Warning(Exam mExam) {
        this.mExam = mExam;
    }
    
    
    
    @Override
    public void run() {
        System.out.println("warning: "+ new Date());
        JOptionPane.showMessageDialog(null, "Only "+ mExam.getmWarning()+ " minutes left.", "warning", JOptionPane.WARNING_MESSAGE);
    }
    
}
