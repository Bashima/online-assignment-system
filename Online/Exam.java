/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package online;

import java.nio.file.attribute.FileTime;

/**
 *
 * @author bashimaislam
 */
public class Exam {
    
    private String mExamName;
    private String mQuesLocation;
    private String mQuestionName;
    private int mStudentId; 
    private String mStartTime;
    private int mDuration;
   private long mBackupInterval;
    private String mRules;
    private int mWarning;
    private FileTime mCreationTimeFirst;
    private FileTime mLastCreationTime;
    public Exam() {
    }

    public int getmWarning() {
        return mWarning;
    }

    public void setmWarning(int mWarning) {
        this.mWarning = mWarning;
    }

    public void setmLastCreationTime(FileTime mLastCreationTime) {
        this.mLastCreationTime = mLastCreationTime;
    }

    public FileTime getmLastCreationTime() {
        return mLastCreationTime;
    }
    

    public FileTime getmCreationTimeFirst() {
        return mCreationTimeFirst;
    }

    public void setmCreationTimeFirst(FileTime mCreationTimeFirst) {
        this.mCreationTimeFirst = mCreationTimeFirst;
    }
    

    
    public long getmBackupInterval() {
        return mBackupInterval;
    }

    public void setmBackupInterval(long mBackupInterval) {
        this.mBackupInterval = mBackupInterval;
    }

    
    public void setmStudentId(int mStudentId) {
        this.mStudentId = mStudentId;
    }

    public int getmStudentId() {
        return mStudentId;
    }

    public String getmQuestionName() {
        return mQuestionName;
    }

    public void setmQuestionName(String mQuestionName) {
        this.mQuestionName = mQuestionName;
    }

    public String getmExamName() {
        return mExamName;
    }

    public String getmQuesLocation() {
        return mQuesLocation;
    }

    public String getmStartTime() {
        return mStartTime;
    }

    public void setmExamName(String mExamName) {
        this.mExamName = mExamName;
    }

    public void setmQuesLocation(String mQuesLocation) {
        this.mQuesLocation = mQuesLocation;
    }

    public void setmStartTime(String mStartTime) {
        this.mStartTime = mStartTime;
    }

    public void setmDuration(int mDuration) {
        this.mDuration = mDuration;
    }

    public void setmRules(String mRules) {
        this.mRules = mRules;
    }

    public int getmDuration() {
        return mDuration;
    }

    public String getmRules() {
        return mRules;
    }

    @Override
    public String toString() {
        return mStudentId  + "," + mCreationTimeFirst + "," + mLastCreationTime;
    }

    
    
    
}
