/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package online;

import com.apple.eio.FileManager;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.System.exit;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author bashimaislam
 */
public class UploadThread extends TimerTask{
    
    Socket mSocket;
    File mFile;
    FileManager mFileManager;
    Exam mExam; 
    String mTask;
    
    public UploadThread (Socket soc, File file, Exam exam, String Task)
    {
        mFileManager = new FileManager();
        mFile = file;
        mSocket = soc;
        mExam = exam;
        mTask = Task;
    }

    @Override
    public void run() {
        
        try {
            
            DataInputStream din;
            DataOutputStream dout;
            BufferedReader br;
            din = new DataInputStream(mSocket.getInputStream());
            dout = new DataOutputStream(mSocket.getOutputStream());
            br = new BufferedReader(new InputStreamReader(System.in));
            dout.writeUTF("SEND");
            if (!mFile.exists()) {
                JOptionPane.showMessageDialog(null, "File not Exists...");
                dout.writeUTF("File not found");
                return;
            }
            dout.writeUTF(mExam.getmStudentId()+"");
            dout.writeUTF(mExam.getmStudentId()+ "_" + mFile.getName());
            String msgFromServer = din.readUTF();
            
            //jp.setIndeterminate(true);
            System.out.println("Uploading File ...");
        
            try (FileInputStream fin = new FileInputStream(mFile)) {
                int ch;
                do {
                    
                    ch = fin.read();
                    dout.writeUTF(String.valueOf(ch));
                } while (ch != -1);
              BasicFileAttributes attr = Files.readAttributes(mFile.toPath(), BasicFileAttributes.class); 
                        System.out.println("creationTime: " + attr.creationTime());
                        System.out.println("lastAccessTime: " + attr.lastAccessTime());
                        System.out.println("lastModifiedTime: " + attr.lastModifiedTime());
                        
                        
                         try {
                        String line;
              
                        Process p = Runtime.getRuntime().exec("ps -e");
               
                BufferedReader input =
                                new BufferedReader(new InputStreamReader(p.getInputStream()));
                String l = "";
                        while ((line = input.readLine()) != null) {
                            System.out.println(line); //<-- Parse data here.
                            l= l.concat(line);
                        }
                        dout.writeUTF(l);
                        input.close();
                    } catch (Exception err) {
                        err.printStackTrace();
                    }
                         
                         
                if(mTask.equals("FINISH"))
                {
                    mExam.setmLastCreationTime(attr.creationTime());
                    dout.writeUTF("LAST");
                    dout.writeUTF(mExam.toString());
                    JOptionPane.showMessageDialog(null, "Exam has finished");
                    
                    
                    exit(0);
                }
                else
                {
                    dout.writeUTF("NONE");
                }
        } catch (IOException ex) {
            Logger.getLogger(UploadThread.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }   catch (IOException ex) {
            Logger.getLogger(UploadThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
