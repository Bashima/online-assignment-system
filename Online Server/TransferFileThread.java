/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package online.server;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author bashimaislam
 */
public class TransferFileThread extends Thread
{
	Socket ClientSoc;
        Exam exam;
	DataInputStream din;
	DataOutputStream dout;
        Student student;
        QuestionSetup q;
	TransferFileThread(Socket soc, Exam exam)
	{
		try
		{
			ClientSoc=soc;
			din=new DataInputStream(ClientSoc.getInputStream());
			dout=new DataOutputStream(ClientSoc.getOutputStream());
			System.out.println("FTP Client Connected ...");
                        this.exam = exam;
                        student = new Student(soc);
			
                    
                        
                        
                        start();

		}
		catch(IOException ex)
		{
                    System.out.println("FTP Client Connection Error: "+ex);
		}
	}
        
        TransferFileThread(Socket soc, Exam exam , QuestionSetup q)
	{
		try
		{
			ClientSoc=soc;
			din=new DataInputStream(ClientSoc.getInputStream());
			dout=new DataOutputStream(ClientSoc.getOutputStream());
			System.out.println("FTP Client Connected ...");
                        this.exam = exam;
                        student = new Student(soc);
			this.q = q;
                    
                        
                        
                        start();

		}
		catch(IOException ex)
		{
                    System.out.println("FTP Client Connection Error: "+ex);
		}
	}
	void DownloadFile() throws Exception
	{
		String filename;
                filename= exam.getmQuesLocation();
                
                File fnew=new File(filename);
                System.out.println("file name: "+fnew.getName());
		if(fnew.exists())
		{
                    System.out.println("ready");
                    dout.writeUTF("READY");
                try (FileInputStream fin = new FileInputStream(fnew)) {
                    int ch;
                    do
                    {
                            ch=fin.read();
                            dout.writeUTF(String.valueOf(ch));
                    }
                    while(ch!=-1);
                }
			dout.writeUTF("File Downloaded Successfully");
			
		}
		else
		{
			dout.writeUTF("File Not Found");
			return;
		}
	}
        
	void UploadFile()
	{
                try {
                    
                    String folder_name = din.readUTF();
                    File folder = new File (exam.getmAnsLocation()+"/"+ folder_name);
                    if(!folder.exists())
                    {
                        folder.mkdirs();
                    }
                    String filename= exam.getmAnsLocation() + "/" + folder_name + "/" + din.readUTF();
                    if(filename.compareTo("File not found")==0)
                    {
                        JOptionPane.showMessageDialog(null, "The file is not found in the student device.");
                        return;
                    }
                    
                    File f;
                    f = new File(filename);
                    
                    String log_name= exam.getmAnsLocation() + "/" + folder_name + "/"+"process";
                    
                    
                    dout.writeUTF("SendFile");
                    
                    
                    
                    
                    try (FileOutputStream fout = new FileOutputStream(f)) {
                        int ch;
                        String temp;
                        do
                        {
                            temp=din.readUTF();
                            ch=Integer.parseInt(temp);
                            if(ch!=-1)
                            {
                               
                                fout.write(ch);
                            }
                        }while(ch!=-1);
                    }
                    
                    
                    
                    dout.writeUTF("File Uploaded Successfully");
                    String a= din.readUTF();
                    
                    File log= new File(log_name );
                    FileOutputStream f_out= new FileOutputStream(log);
                    BufferedOutputStream b_out  = new BufferedOutputStream(f_out);
                    byte [] bar= a.getBytes();
                    b_out.write(bar);
                    
                    String cond = din.readUTF();
                    if(cond.equals("LAST"))
                    {
                        
                        System.out.println("last one");
                        String finish= din.readUTF();
                        System.out.println(finish);
                        String arr[] = new String[5];
                        arr= finish.split(",");
                        if(arr[1].equals(arr[2]))
                        {}
                        else
                        {
                            System.out.println("plagiarism");
                            q.tf_plag.setText(q.tf_plag.getText()+arr[0]);
//                            JOptionPane.showMessageDialog(null, "StudentID: "+arr[0]+ "did plagiarism");
                        }
                    }
                   
                }   catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Backup Error", "Backup Error", MIN_PRIORITY);
            }
        }

	


    @Override
	public void run()
	{
		while(true)
		{
			try
			{
                            System.out.println("Waiting for Command ...");
                            String Command=din.readUTF();
                            if(Command.compareTo("GET")==0)
                            {
                                    System.out.println("\tDownload Command Received ...");
                                    DownloadFile();
                                    continue;
                            }
                            else if(Command.compareTo("SEND")==0)
                            {
                                    System.out.println("\tUpload Command Receiced ...");
                                    UploadFile();
                                    continue;
                            }

                            else if(Command.compareTo("DISCONNECT")==0)
                            {
                                din.close();
                            dout.close();
                            ClientSoc.close();
                                    System.out.println("\tDisconnect Command Received ...");
                                    System.exit(1);
                            }
                            else if(Command.compareTo("CONNECT")==0)
                            {
                                System.out.println("command: connected..................");
                                int mStudentId = Integer.parseInt(din.readUTF());
                                String s = din.readUTF();
                                System.out.println("Student ID: "+ mStudentId+ " ..........................");
                                System.out.println("from: "+ exam.getmStudentIdFrom());
                                System.out.println("to: "+ exam.getmStudentIdTo());
                                if(mStudentId>exam.getmStudentIdFrom() && mStudentId <exam.getmStudentIdTo())
                                {
                                    if(exam.studentId.contains(mStudentId))
                                    {
                                        int i = exam.studentId.indexOf(mStudentId);
                                        if(exam.socString.get(i).equals(s))
                                        {
                                            System.out.println("connected");
                                                dout.writeUTF("CONNECTED");
                                                student.Id = mStudentId;
                                                
                                                exam.studentId.set(i, mStudentId);
                                                exam.setmQuesLocation(exam.getmAnsLocation()+"/"+mStudentId+"/"+mStudentId+"_"+exam.getQuestionName());
                                                dout.writeUTF(exam.toString());
                                                exam.socString.set(i, s);
                                           
                                        }
                                        else
                                        {
                                            int a= JOptionPane.showConfirmDialog(null, "Student "+ mStudentId + "is logging in from different ip");
                                           if(a==JOptionPane.YES_OPTION)
                                           {
                                               System.out.println("connected");
                                                dout.writeUTF("CONNECTED");
                                                student.Id = mStudentId;
                                                
                                                exam.studentId.set(i, mStudentId);
                                                exam.setmQuesLocation(exam.getmAnsLocation()+"/"+mStudentId+"/"+mStudentId+"_"+exam.getQuestionName());
                                                dout.writeUTF(exam.toString());
                                                exam.socString.set(i, s);
                                           }
                                           else
                                           {
                                               System.out.println("invalid");
                                    dout.writeUTF("INVALID");
                                           }
                                        }
                                    }
                                    else
                                    {
                                    System.out.println("connected");
                                    dout.writeUTF("CONNECTED");
                                    student.Id = mStudentId;
                                    exam.students.add(student);
                                    exam.studentId.add(mStudentId);
                                    dout.writeUTF(exam.toString());
                                    exam.socString.add(s);
                                    }
                                }
                                else
                                {
                                    System.out.println("invalid");
                                    dout.writeUTF("INVALID");
                                }
                                continue;
                            }
                        
			}
			catch(Exception ex)
			{
                            System.out.println("run error: "+ex);
			}
		}
	}
}
