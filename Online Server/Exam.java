/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package online.server;

import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author bashimaislam
 */
public class Exam {
    private int mExamId;
    private String mExamName;
    private String mQuesLocation;
    private String mAnsLocation;
    private int mStudentIdTo;
    private int mStudentIdFrom;
    private String mStartTime;
    private int mDuration;
    private int mBackupInterval;
    private String mRulesLocation;
    private String QuestionName;
    private String mRules;
    private int mWarning;
    private int[] mStudentArray;
    public static ArrayList students=new ArrayList<Student>();
    public static ArrayList sockList=new ArrayList<Socket>();
    public static ArrayList socString = new ArrayList<String>();
    public static ArrayList studentId= new ArrayList<Integer>();
    
    
    public Exam() {
    }

    public void initialmStudentArray(int mStudentArray) {
        this.mStudentArray = new int[10];
    }

    public void setmStudentArray(int[] mStudentArray) {
        this.mStudentArray = mStudentArray;
    }
    
    
    

    public void setmWarning(int mWarning) {
        this.mWarning = mWarning;
    }

    public int getmWarning() {
        return mWarning;
    }

    public void setmRules(String mRules) {
        this.mRules = mRules;
    }

    public String getmRules() {
        return mRules;
    }

    public String getQuestionName() {
        return QuestionName;
    }

    public void setQuestionName(String QuestionName) {
        this.QuestionName = QuestionName;
    }

    
    

    public int getmExamId() {
        return mExamId;
    }

    public String getmExamName() {
        return mExamName;
    }

    public String getmQuesLocation() {
        return mQuesLocation;
    }

    public String getmAnsLocation() {
        return mAnsLocation;
    }

    public int getmStudentIdTo() {
        return mStudentIdTo;
    }

    public int getmStudentIdFrom() {
        return mStudentIdFrom;
    }

    public String getmStartTime() {
        return mStartTime;
    }

    public int getmDuration() {
        return mDuration;
    }

    public int getmBackupInterval() {
        return mBackupInterval;
    }

    public String getmRulesLocation() {
        return mRulesLocation;
    }

    public void setmExamId(int mExamId) {
        this.mExamId = mExamId;
    }

    public void setmExamName(String mExamName) {
        this.mExamName = mExamName;
    }

    public void setmQuesLocation(String mQuesLocation) {
        this.mQuesLocation = mQuesLocation;
    }

    public void setmAnsLocation(String mAnsLocation) {
        this.mAnsLocation = mAnsLocation;
    }

    public void setmStudentIdTo(int mStudentIdTo) {
        this.mStudentIdTo = mStudentIdTo;
    }

    public void setmStudentIdFrom(int mStudentIdFrom) {
        this.mStudentIdFrom = mStudentIdFrom;
    }

    public void setmStartTime(String mStartTime) {
        this.mStartTime = mStartTime;
    }

    public void setmDuration(int mDuration) {
        this.mDuration = mDuration;
    }

    public void setmBackupInterval(int mBackupInterval) {
        this.mBackupInterval = mBackupInterval;
    }

    public void setmRulesLocation(String mRulesLocation) {
        this.mRulesLocation = mRulesLocation;
    }

    @Override
    public String toString() {
        return mExamName + "," + mStartTime + "," + mDuration + "," +  mRules + "," + QuestionName + "," + mBackupInterval+ ","+ mWarning;
    }
    
    
}
