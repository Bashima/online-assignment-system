/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package online.server;

import java.net.Socket;

/**
 *
 * @author bashimaislam
 */
public class Student {
    
    public int Id;
    public Socket Soc;
    public Boolean Plagiarism;
    public String FirstCreationDate;
    public String LastCreationDate;

    public Student(int Id, Socket Soc, Boolean Plagiarism, String FirstCreationDate, String LastCreationDate) {
        this.Id = Id;
        this.Soc = Soc;
        this.Plagiarism = Plagiarism;
        this.FirstCreationDate = FirstCreationDate;
        this.LastCreationDate = LastCreationDate;
    }

    public Student(int Id) {
        this.Id = Id;
    }

    public Student(Socket Soc) {
        this.Soc = Soc;
    }

    public Student(int Id, Socket Soc) {
        this.Id = Id;
        this.Soc = Soc;
    }

    
    
    
    
}
